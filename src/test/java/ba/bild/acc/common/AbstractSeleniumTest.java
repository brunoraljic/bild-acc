package ba.bild.acc.common;

import java.util.List;

import org.jbehave.core.annotations.AfterStories;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Abstract selenium test is dealing with web driver ({@link FirefoxDriver}). It
 * also contains common methods used in all Selenium tests in the project.
 * 
 */
public class AbstractSeleniumTest {

	private static final String APP_LOCATION = "http://codecentric.de/";
	protected static final String SEARCH_WITH_NO_RESULT = "23444444446426262";
	protected static final String SEARCH_WITH_RESULT = "java";

	private static WebDriver driver;

	public WebDriver getDriver() {
		if (driver == null) {
			final FirefoxProfile firefoxProfile = new FirefoxProfile();
			// Prevent opening "Try new Incognito mode"
			firefoxProfile.setPreference("xpinstall.signatures.required", false);
			driver = new FirefoxDriver(firefoxProfile);
		}
		return driver;
	}

	@AfterStories
	public void quitWebDriver() {
		if (driver != null) {
			driver.quit();
		}
	}

	/**
	 * Click element identified by id
	 * 
	 * @param id
	 */
	protected void clickById(String id) {
		findElementById(id).click();
	}

	/**
	 * Fill the input field
	 * 
	 * @param name
	 *            for identifying input field
	 * @param text
	 *            to be typed in
	 */
	protected void typeInByName(String name, String text) {
		getDriver().findElement(By.name(name)).sendKeys(text);
	}

	/**
	 * Fill the input field
	 * 
	 * @param id
	 * @param text
	 */
	protected void typeInById(String id, String text) {
		getDriver().findElement(By.id(id)).sendKeys(text);
	}

	/**
	 * Open specific page in the application
	 * 
	 * @param page
	 *            is only a part of the link, because it already contains the
	 *            first part
	 */
	protected void openPage(String page) {
		getDriver().get(APP_LOCATION + page);
	}

	/**
	 * Find element by css class name
	 * 
	 * @param className
	 * @return
	 */
	protected WebElement findElementByClassName(String className) {
		return getDriver().findElement(By.className(className));
	}

	/**
	 * Show the current url location
	 * 
	 * @return
	 */
	protected String getCurrentURL() {
		return getDriver().getCurrentUrl();
	}

	/**
	 * Wait until element is visible
	 * 
	 * @param id
	 *            to locate the element
	 */
	protected void waitUntilVisibleById(String id) {
		WebDriverWait wait = getWebDriver();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
	}

	protected void waitUntilVisibleByClassName(String className) {
		WebDriverWait wait = getWebDriver();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(className)));
	}

	protected void waitUntilVisibleByTagName(String tagName) {
		WebDriverWait wait = getWebDriver();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName(tagName)));
	}

	/**
	 * Get web driver for waiting, and the timeout is set to 10 seconds
	 * 
	 * @return
	 */
	private WebDriverWait getWebDriver() {
		return new WebDriverWait(getDriver(), 10);
	}

	/**
	 * Check if the page source contains the specified text
	 * 
	 * @param text
	 * @return
	 */
	protected boolean pageSourceContains(String text) {
		return getDriver().getPageSource().contains(text);
	}

	/**
	 * Find element by id
	 * 
	 * @param id
	 * @return
	 */
	protected WebElement findElementById(String id) {
		return getDriver().findElement(By.id(id));
	}

	/**
	 * Find all elements identified by the xpath locator
	 * 
	 * @param xpath
	 * @return
	 */
	protected List<WebElement> findAllElementsByXpath(String xpath) {
		return getDriver().findElements(By.xpath(xpath));
	}

	/**
	 * Find element by name
	 * 
	 * @param name
	 * @return
	 */
	protected WebElement findElementByName(String name) {
		return getDriver().findElement(By.name(name));
	}

	/**
	 * Finds elements by id
	 * 
	 * @param id
	 * @return
	 */
	protected List<WebElement> findAllElementsById(String id) {
		return getDriver().findElements(By.id(id));
	}

	/**
	 * Finds all elements by class name
	 * 
	 * @param className
	 * @return
	 */
	protected List<WebElement> findAllElementsByClassName(String className) {
		return getDriver().findElements(By.className(className));
	}

}
