package ba.bild.acc.common;

import java.util.Arrays;

import org.jbehave.core.configuration.spring.SpringStoryControls;
import org.jbehave.core.configuration.spring.SpringStoryReporterBuilder;
import org.jbehave.core.embedder.EmbedderClassLoader;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.parsers.RegexPrefixCapturingPatternParser;
import org.jbehave.core.reporters.Format;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ba.bild")
public class ApplicationContextConfiguration {

	@Bean
	public LoadFromClasspath loadFromClasspath() {
		return new LoadFromClasspath(new EmbedderClassLoader(Arrays.asList("build/classes")));
	}

	@Bean
	public RegexPrefixCapturingPatternParser regexPrefixCapturingPatternParser() {
		return new RegexPrefixCapturingPatternParser("$");
	}

	@Bean
	public SpringStoryControls springStoryControls() {
		SpringStoryControls springStoryControls = new SpringStoryControls();
		springStoryControls.setDryRun(false);
		springStoryControls.setSkipScenariosAfterFailure(false);
		return springStoryControls;
	}

	@Bean(destroyMethod = "withDefaultFormats")
	public SpringStoryReporterBuilder springStoryReporterBuilder() {
		SpringStoryReporterBuilder springStoryReporterBuilder = new SpringStoryReporterBuilder();
		springStoryReporterBuilder.setFormats(Arrays.asList(Format.CONSOLE, Format.HTML));
		return springStoryReporterBuilder;
	}

}
