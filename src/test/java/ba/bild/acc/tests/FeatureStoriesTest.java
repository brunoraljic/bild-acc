package ba.bild.acc.tests;

import static org.jbehave.core.io.CodeLocations.codeLocationFromPath;

import java.util.List;

import org.jbehave.core.io.StoryFinder;

import ba.bild.acc.common.AbstractAcceptanceTest;

public class FeatureStoriesTest extends AbstractAcceptanceTest {

	protected List<String> storyPaths() {
		return new StoryFinder().findPaths(codeLocationFromPath("src/test/resources"), "stories/general/general.story",
				"");
	}

}
