package ba.bild.acc.feature;

import org.jbehave.core.annotations.When;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import ba.bild.acc.common.AbstractSeleniumTest;
import ba.bild.acc.common.Steps;

@Steps
public class FeatureWhen extends AbstractSeleniumTest {

	@When("user clicks Blog button")
	public void clickBlogButton() {
		getDriver().findElement(By.linkText("Blog")).click();
	}

	@When("user search for unknown term")
	public void searchForSomethingWithoutResults() {
		findElementByClassName("searchform").click();
		waitUntilVisibleByClassName("header-search");
		getDriver().findElement(By.className("search-input")).sendKeys(SEARCH_WITH_NO_RESULT);
		getDriver().findElement(By.className("search-input")).sendKeys(Keys.ENTER);
	}

	@When("user search for java")
	public void searchForSomethingWithResults() {
		// TODO do this with parameterized configuration
		findElementByClassName("searchform").click();
		waitUntilVisibleByClassName("header-search");
		getDriver().findElement(By.className("search-input")).sendKeys(SEARCH_WITH_RESULT);
		getDriver().findElement(By.className("search-input")).sendKeys(Keys.ENTER);
	}

	@When("user submits empty form")
	public void userSubmitsEmptyForm() {
		getDriver().findElement(By.name("Absenden")).sendKeys(Keys.ENTER);
	}

	@When("user submits empty form with first checkbox selected")
	public void userSubmitsEmptyFormWithFirstCheckbox() {
		getDriver().findElement(By.xpath("//label[@for='formular_aufmerksam_persoenlich']")).click();

		// Instead of hitting enter, you can simply submit form
		getDriver().findElement(By.name("Absenden")).submit();
	}

}
