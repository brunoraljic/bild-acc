package ba.bild.acc.feature;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;

import org.jbehave.core.annotations.Then;
import org.openqa.selenium.By;

import ba.bild.acc.common.AbstractSeleniumTest;
import ba.bild.acc.common.Steps;

@Steps
public class FeatureThen extends AbstractSeleniumTest {

	@Then("codecentric.de Blog is presented")
	public void presentCcBlog() {
		// Real class name is "active", change it to see how it will be shown in
		// the report
		String expectedClassName = "active";
		String className = getDriver().findElement(By.linkText("Blog")).getAttribute("class");
		assertTrue(pageSourceContains("Continuous Delivery on Steroids: Einführung in Heroku Pipelines"));
		assertEquals(expectedClassName, className);
	}

	@Then("no result message is shown")
	public void showNoResultMessage() {
		String expectedH3Text = "Nichts gefunden, was den Suchkriterien entspricht.";
		String expectedH2Text = String.format("Suche nach \"%s\"", SEARCH_WITH_NO_RESULT);

		waitUntilVisibleByTagName("h2");
		assertEquals(expectedH2Text, getDriver().findElement(By.tagName("h2")).getText());
		String h3Text = getDriver().findElement(By.tagName("h3")).getText();
		assertEquals(expectedH3Text, h3Text);
	}

	@Then("search results are shown")
	public void showSearchResults() {
		String expectedH2Text = String.format("Suche nach \"%s\"", SEARCH_WITH_RESULT);
		String expectedH3Text = "Java Performance Tools, Teil 1";
		waitUntilVisibleByTagName("h2");
		assertEquals(expectedH2Text, getDriver().findElement(By.tagName("h2")).getText());
		// Pay attention to the method findElementS (plural)
		assertEquals(expectedH3Text, getDriver().findElements(By.tagName("h3")).get(0).getText());
	}

	@Then("errors for contact form are shown")
	public void showErrorsForContactForm() {
		waitUntilVisibleByClassName("form-error");

		assertEquals("Senden fehlgeschlagen!",
				getDriver().findElement(By.xpath("/html/body/div[1]/div[4]/div/div/div/form/div[1]/strong")).getText());
		// you can play with xpath, this is the same thing
		// google xpath!
		assertEquals("Senden fehlgeschlagen!",
				getDriver().findElement(By.xpath("//div[contains(@class, 'form-error')]/strong")).getText());
		assertEquals("Bitte sagen Sie uns, wie sie auf uns aufmerksam geworden sind", getDriver()
				.findElement(By.xpath("/html/body/div[1]/div[4]/div/div/div/form/div[1]/ul/li[1]")).getText());

		// checking disabled elements
		assertFalse(getDriver().findElement(By.className("searchform-overlay")).isDisplayed());
		// Doing this just for presenting purposes
		findElementByClassName("searchform").click();
		assertTrue(getDriver().findElement(By.className("searchform-overlay")).isDisplayed());

	}

	@Then("errors for contact form are shown without error for first checkbox")
	public void showErrorsForContactFormWithFirstCheckbox() {
		waitUntilVisibleByClassName("form-error");

		assertEquals("Senden fehlgeschlagen!",
				getDriver().findElement(By.xpath("//div[contains(@class, 'form-error')]/strong")).getText());
		assertEquals("Bitte geben Sie den Vornamen an.", getDriver()
				.findElement(By.xpath("/html/body/div[1]/div[4]/div/div/div/form/div[1]/ul/li[1]")).getText());

	}

}
