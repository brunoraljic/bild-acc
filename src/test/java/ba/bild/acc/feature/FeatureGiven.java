package ba.bild.acc.feature;

import org.jbehave.core.annotations.Given;

import ba.bild.acc.common.AbstractSeleniumTest;
import ba.bild.acc.common.Steps;

@Steps
public class FeatureGiven extends AbstractSeleniumTest {

	@Given("codecentric.de page")
	public void openCodecentricPage() {
		openPage("");
	}

	@Given("codecentric.de kontakt page")
	public void openCodecentricContactPage() {
		openPage("kontakt/");
	}

}
