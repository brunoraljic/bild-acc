Narative:
Test codecentric.de page

Scenario: Test codecentric.de blog availability
Given codecentric.de page
When user clicks Blog button
Then codecentric.de Blog is presented

Scenario: Test codecentric.de search functionality - no results
Given codecentric.de page
When user search for unknown term
Then no result message is shown

Scenario: Test codecentric.de search functionality - with results
Given codecentric.de page
When user search for java
Then search results are shown

Scenario: Test codecentric.de contact form - empty form
Given codecentric.de kontakt page
When user submits empty form
Then errors for contact form are shown

Scenario: Test codecentric.de contact form - empty form - first checkbox
Given codecentric.de kontakt page
When user submits empty form with first checkbox selected
Then errors for contact form are shown without error for first checkbox